package cagin.com.fallingwordsproject.base;

import android.content.Context;
import android.media.MediaPlayer;

import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;

import cagin.com.fallingwordsproject.R;

public class Utility {

    public void correctSound(Context context){
        int resID=context.getResources().getIdentifier("correct_sound", "raw", context.getPackageName());
        MediaPlayer mediaPlayer=MediaPlayer.create(context,resID);
        mediaPlayer.start();
    }

    public void wrongSound(Context context){
        int resID=context.getResources().getIdentifier("fail_sound", "raw", context.getPackageName());
        MediaPlayer mediaPlayer=MediaPlayer.create(context,resID);
        mediaPlayer.start();
    }

    public void checkInternetConnection(Context context){
        new CDialog(context).createAlert(context.getString(R.string.check_internet),
                CDConstants.ERROR,   // Type of dialog
                CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                .setDuration(5000)   // in milliseconds
                .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();
    }
}
