package cagin.com.fallingwordsproject.base;

import android.app.Application;
import android.content.Context;

import cagin.com.fallingwordsproject.data.di.AppComponent;
import cagin.com.fallingwordsproject.data.di.DaggerAppComponent;
import cagin.com.fallingwordsproject.data.di.RetrofitModule;

public class FallingWordsApplication extends Application {

    private static AppComponent appComponent;

    private static FallingWordsApplication get(Context context){
        return (FallingWordsApplication) context.getApplicationContext();
    }

    public static FallingWordsApplication create(Context context) {
        return FallingWordsApplication.get(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
    }

    public AppComponent buildAppComponent() {
        return DaggerAppComponent.builder().retrofitModule(new RetrofitModule()).build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}