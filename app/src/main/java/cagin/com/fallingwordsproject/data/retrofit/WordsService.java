package cagin.com.fallingwordsproject.data.retrofit;

import java.util.List;

import cagin.com.fallingwordsproject.base.Constants;
import cagin.com.fallingwordsproject.data.model.LangItem;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WordsService {
    @GET(Constants.Api.WORDS)
    Call<List<LangItem>> getWordsList();
}

