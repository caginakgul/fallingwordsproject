package cagin.com.fallingwordsproject.data.di;

import javax.inject.Singleton;

import cagin.com.fallingwordsproject.data.repository.LanguageRepository;
import dagger.Component;

@Component(modules = {RetrofitModule.class})
@Singleton
public interface AppComponent {
    void inject(LanguageRepository languageRepository);
}
