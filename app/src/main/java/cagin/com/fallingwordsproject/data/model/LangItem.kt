package cagin.com.fallingwordsproject.data.model

data class LangItem (val text_eng: String, val text_spa: String)