package cagin.com.fallingwordsproject.data.repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import cagin.com.fallingwordsproject.base.FallingWordsApplication;
import cagin.com.fallingwordsproject.data.model.LangItem;
import cagin.com.fallingwordsproject.data.retrofit.WordsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguageRepository {

    @Inject
    WordsService service;
    FallingWordsApplication appController;

    public void sendWordsRequest(Context context, final MutableLiveData<List<LangItem>> data) {
        appController = FallingWordsApplication.create(context);
        appController.getAppComponent().inject(this);
        service.getWordsList().enqueue(new Callback<List<LangItem>>() {
            @Override
            public void onResponse(Call<List<LangItem>> call, Response<List<LangItem>> response) {
                List<LangItem> serviceResult = response.body();
                if(serviceResult==null){
                    data.setValue(null);
                }else{
                    data.setValue(serviceResult);
                }
            }
            @Override
            public void onFailure(Call<List<LangItem>> call, Throwable t) {
                data.setValue(null);
            }
        });
    }
}