package cagin.com.fallingwordsproject.game;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;

import java.util.List;
import java.util.Random;

import cagin.com.fallingwordsproject.R;
import cagin.com.fallingwordsproject.base.Utility;
import cagin.com.fallingwordsproject.data.model.LangItem;
import cagin.com.fallingwordsproject.databinding.ActivityGameBinding;


//TODO context yollama işini bırak viewmodelde context olmamalı
//TODO Viewmodelde new ile yarattığın languageRepository'i dagger ile yaratmalısın.
//TODO bu sınıfı kontrol et gereksiz new'lerden  - utility - method her çağırıldığında yeni yaratılan objelerden kurtul. utility olabilir mi bak
//TODO onclick işini düzelt
//TODO cevap vermezse fail ettirt
public class GameActivity extends AppCompatActivity {
    private ActivityGameBinding activityGameBinding;
    private Animation slideDownAnimation;
    private GameViewModel gameViewModel;
    private String[] spanishPrediction;
    private int score=0;
    private Utility utility;
    private List<LangItem> wordList;
    private int endGame=0;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        init();
        gameViewModel.sendLangRequest(this);
        gameViewModel.getWordsList().observe(this, new Observer<List<LangItem>>() {
            @Override
            public void onChanged(@Nullable List<LangItem> networkResponse) {
                wordList=networkResponse;
                if(wordList!=null){
                    startGame();
                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.check_internet),Toast.LENGTH_SHORT).show();
                }
            }
        });
        activityGameBinding.btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkResult(true);
            }
        });
        activityGameBinding.btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkResult(false);
            }
        });
        activityGameBinding.tvPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
    }

    private void init(){
        activityGameBinding = DataBindingUtil.setContentView(this, R.layout.activity_game);
        gameViewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down_animation);
        spanishPrediction= new String[2];
        utility=new Utility();
        activityGameBinding.tvPlay.setVisibility(View.GONE);
    }

    /*
    This method selects an English word from the list.
    An array keeps two Spanish word, one of them is the true answer and the other is false.
    Random int number decides which Spanish word from the array will be displayed to the user.
    With that way 50% the Spanish word and the English one will be matched.
    spanishPrediction[0]->correct spanish word
    spanishPrediction[1]->after random selection, it should be correct or wrong
     */
    private void startGame(){
        if(endGame<5){
            startTimer();
            Random rand = new Random();
            int max=wordList.size()-1;
            int randomNum = rand.nextInt((max-0)+1)+0;
            activityGameBinding.tvEng.setText(wordList.get(randomNum).getText_eng());
            spanishPrediction[0]=wordList.get(randomNum).getText_spa();
            randomNum = rand.nextInt((max-0)+1)+0;
            spanishPrediction[1]=wordList.get(randomNum).getText_spa();
            randomNum = rand.nextInt((1 - 0) + 1) + 0;
            activityGameBinding.tvSpa.setText(spanishPrediction[randomNum]);
            activityGameBinding.tvSpa.startAnimation(slideDownAnimation);
            endGame++;
        }else{
            endGame();
        }
    }

    private void checkResult(Boolean userAnswer){
        countDownTimer.cancel();
        if(userAnswer){//user assert the words match-thinks correct
            if(spanishPrediction[0].equals(activityGameBinding.tvSpa.getText())){//the word in tv was correct true answer
                Log.d("answer","true");
                score++;
                utility.correctSound(this);
                activityGameBinding.tvSpa.clearAnimation();
                activityGameBinding.simpleRatingBar.setRating(score);
            }else{//wrong answer
                utility.wrongSound(this);
                activityGameBinding.tvSpa.clearAnimation();
            }
        }else{
            if(spanishPrediction[0].equals(activityGameBinding.tvSpa.getText())){//the word in tv was correct, wrong answer
                utility.wrongSound(this);
                activityGameBinding.tvSpa.clearAnimation();
            }else{//true answer
                score++;
                utility.correctSound(this);
                activityGameBinding.tvSpa.clearAnimation();
                activityGameBinding.simpleRatingBar.setRating(score);
            }
        }
        startGame();
    }

    private void endGame(){
        Log.d("endGame","runned");
        activityGameBinding.tvSpa.setVisibility(View.GONE);
        activityGameBinding.tvEng.setVisibility(View.GONE);
        activityGameBinding.btCancel.setVisibility(View.GONE);
        activityGameBinding.btConfirm.setVisibility(View.GONE);
        activityGameBinding.tvPlay.setVisibility(View.VISIBLE);

        new CDialog(this).createAlert(getString(R.string.complete)+" "+score+"/5",
                CDConstants.SUCCESS,   // Type of dialog
                CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                .setDuration(5000)   // in milliseconds
                .setTextSize(CDConstants.LARGE_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();

    }

    private void startTimer(){
        countDownTimer=new CountDownTimer(6000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("countDown","tick");
            }
            public void onFinish() {
                Log.d("countDown","onFinish");
               startGame();
            }
        }.start();
    }
}

