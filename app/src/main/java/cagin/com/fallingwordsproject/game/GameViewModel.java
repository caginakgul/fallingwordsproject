package cagin.com.fallingwordsproject.game;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import cagin.com.fallingwordsproject.data.model.LangItem;
import cagin.com.fallingwordsproject.data.repository.LanguageRepository;

public class GameViewModel extends AndroidViewModel {
    private MutableLiveData<List<LangItem>> wordsObservable;

   /* @Inject
    LanguageRepository languageRepository;*/

    @Inject
    public GameViewModel(@NonNull Application application){
        super(application);
        wordsObservable= new MutableLiveData<>();
    }

    public void sendLangRequest(Context context){
        LanguageRepository languageRepository = new LanguageRepository();
        languageRepository.sendWordsRequest(context, wordsObservable);
        Log.d("hodor","hodor");
    }

    public LiveData<List<LangItem>> getWordsList(){
        return wordsObservable;
    }
}
